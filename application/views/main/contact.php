<div class="container" >

    <h3>Where To Find Us?</h3>
    <div class="googleMap">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1594.9907270120018!2d27.92192401119106!3d47.75344224545888!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2s!4v1570186154471!5m2!1sru!2s"
                width="100%"
                height="450"
                frameborder="0"
                style="border:0;"
                allowfullscreen="">

        </iframe>
    </div>
    <!-- Container (Contact Section) -->
    <div id="contact" class="container">
        <h3 class="text-center">Contact</h3>
        <p class="text-center"><em>Do you love our food?</em></p>

        <div class="row">
            <div class="col-md-4">
                <p>Fan? Drop a note.</p>
                <p><span class="glyphicon glyphicon-map-marker"></span>Balti, MD</p>
                <p><span class="glyphicon glyphicon-phone"></span>Phone: +373 78555555</p>
                <p><span class="glyphicon glyphicon-envelope"></span>Email: mail@mail.com</p>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
                    </div>
                </div>
                <textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea>
                <br>
                <div class="row">
                    <div class="col-md-12 form-group">
                        <button class="btn pull-right" type="submit">Send</button>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>

</div>

<style>
    .container{
       margin-top: 5%;
    }
    .googleMap {
        border:2px solid #ccc;
    }
    textarea {
        resize: none;
    }
</style>