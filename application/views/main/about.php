<div class="comtainer">
    <div id="band" class="container text-center">
        <h3>THE FOOD</h3>
        <p><em>We love eating!</em></p>
        <p>We have created a fictional food website. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
            do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exerc
            itation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit
            in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non p
            roident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed
            do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ex
            ercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        <br>
    </div>

    <!-- Third Container (Grid) -->
    <div class="container-fluid bg-3 text-center">
        <h3 class="margin">Where To Find Us?</h3><br>
        <div class="row">
            <div class="col-sm-4">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <img src="public/assets/img/2.jpg" class="img-responsive margin" style="width:100%" alt="Image">
            </div>
            <div class="col-sm-4">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <img src="public/assets/img/2.jpg" class="img-responsive margin" style="width:100%" alt="Image">
            </div>
            <div class="col-sm-4">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <img src="public/assets/img/2.jpg" class="img-responsive margin" style="width:100%" alt="Image">
            </div>
        </div>
    </div>

</div>

<style>
    .container {
        padding-top: 50px;
    }

    .container-fluid {
        margin-bottom: 30px;
    }
</style>