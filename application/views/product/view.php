<div class="container">
    <h1>One product</h1>
    <div class="product"></div>
</div>

<script>
    $(document).ready(function() {
        $.ajax({
            type: "GET",
            url: '/products/<?= $this->route['id'] ?>',
            dataType: 'JSON',
            success: function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    var $product = response.data;
                    var content = `<div class="card bg-dark text-white">
                                        <img src="/public/assets/img/` + $product[0]['Picture'] + `" class="card-img image" alt="...">
                                        <div class="card-img-overlay">
                                            <h4 class="card-title">` + $product[0]['Name'] + `</h4>
                                            <p class="card-text">Price: <b>` + $product[0]['Price'] + `</b></p>
                                            <p class="card-text">Mass: <b>` + $product[0]['Massa'] + `</b></p>
                                            <a href="/account/add/` + $product[0]['ID'] + `" class="btn btn-warning">Добавить в корзину</a>
                                            <a href="#" class="btn btn-success">Заказать</a>
                                        </div>
                                    </div>`;
                    $('.product').append(content);

                } else {
                    alert('Error with server response');
                }
            }
        });
    });
</script>
<style>
    .container {
        margin-bottom: 50px;
        padding-top: 50px;
    }

    .image {
        max-width: 100%;
    }
</style>