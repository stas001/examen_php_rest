<div class="container">
    <h2>Make order!</h2>
    <div class="row text-center parent"></div>
</div>

<script>
    $(document).ready(function() {
        $.ajax({
            type: "GET",
            url: '/products',
            dataType: 'JSON',
            success: function(response) {
                if (response.status == 200) {
                    var product = response.data;
                    console.log(product)
                    for (var i = 0; i < product.length; i++) {
                        var content = `<div class="col-sm-3">
                                            <div class="thumbnail">
                                                <img src="/public/assets/img/` + product[i]['Picture'] + `" class="card-img"
                                                    alt="` + product[i]['Picture'] + `" width="400" height="400">
                                                <h4>` + product[i]['Name'] + `</h4>
                                                <p>Price: <b>` + product[i]['Price'] + `</b></p>
                                                <p>Massa: <b>` + product[i]['Massa'] + `</b></p>
                                                <a href="/allProducts/` + product[i]['ID'] + `" class="btn btn-primary">Посмотреть</a>
                                            </div>
                                        </div>`;
                        $('.parent').append(content);
                    }

                } else {
                    alert('Error with server response');
                }
            }
        })
    })
</script>
<style>
    .container {
        margin-bottom: 50px;
    }
</style>