<?
return [
    "" => [
        "controller" => "main",
        "action" => "index"
    ],
    'about' => [
        "controller" => "main",
        "action" => "about"
    ],
    'contact' => [
        "controller" => "main",
        "action" => "contact"
    ],
    'products/([0-9]+)' => [
        "controller" => "product",
        "action" => "oneProductAjax",
        "id" => '$1'
    ],
    'allProducts/([0-9]+)' => [
        "controller" => "product",
        "action" => "view",
        "id" => '$1'
    ],
    'products' => [
        "controller" => "product",
        "action" => "productsAjax",
        "id" => '$1'
    ],
    'allProducts' => [
        "controller" => "product",
        "action" => "list",
        "id" => '$1'
    ],




];
