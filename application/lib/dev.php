<?
ini_set("display_errors", 1);
error_reporting(E_ALL);


function debug($str)
{
    echo "<pre>";
    print_r($str);
    echo "</pre>";
}

function requestAjax($data)
{
    if ($data) {
        echo json_encode([
            'status' => 200,
            'data' => $data,
        ]);
        exit;
    }
    header('HTTP/1.0 400 Bad Request');
    exit;
}
