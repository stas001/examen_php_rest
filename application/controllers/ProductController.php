<?

namespace controllers;

use core\Controller;
use core\API;

class ProductController extends Controller
{
    public function actionList()
    {
        $this->view->render('List Products');
    }

    public function actionView()
    {
        $this->view->render('One Product');
    }

    public function actionProductsAjax()
    {
        header("Content-Type: application/json; charset=UTF-8");

        $data = $this->model->getProducts();

        requestAjax($data);
    }

    public function actionOneProductAjax()
    {
        header("Content-Type: application/json; charset=UTF-8");

        $id = $this->route['id'];
        $data = $this->model->getOneProduct($id);

        requestAjax($data);
    }
}
