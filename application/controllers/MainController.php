<?

namespace controllers;

use core\Controller;

class MainController extends Controller
{
    public function actionIndex()
    {
        $this->view->render('Home page');
    }

    public function actionAbout()
    {
        $this->view->render('About page');
    }

    public function actionContact()
    {
        $this->view->render('Contact page');
    }
}
