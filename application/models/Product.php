<?

namespace models;

use core\Model;

class Product extends Model
{
    public function getProducts()
    {
        $querySelect = 'SELECT * FROM products';
        return $this->db->row($querySelect);
    }

    public function getOneProduct($id)
    {
        $querySelect = 'SELECT * FROM products WHERE ID=' . $id;
        return $this->db->row($querySelect);
    }
}
